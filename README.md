Seocraft Network NPC API
===================

This is one of the tools used by the Seocraft Network to manage NPC's in its different services. This API is simplified for use by third parties, including an advanced Reflection system and advanced npc functionalities.

# Licence

Seocraft Network NPC API is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

A copy of the GNU Affero General Public License is included in the file LICENSE.txt, and can also be found at https://www.gnu.org/licenses/agpl-3.0.en.html

The AGPL license is quite restrictive, please make sure you understand it. If you run a modified version of this software as a network service, anyone who can use that service must also have access to the modified source code.

# Functionalities

* `Creation` - Create a NPC in anytime by using: `NpcAPI.createNPC(String name, String value, String signature, Location location, Boolean name_visible, Player receiver)`
* `NameTag visibility` - Hide NPC NameTag by changing name_visible in the creation by `false`
* `Animation and Status` - Play an animation or an status such as `SWING_ARM` using `npc.playAnimation(Animation animation)` or `npc.playStatus(Status status)`
* `Equipment` - Equip the npc with an ItemStack in different positions such as `HELMET`, `CHESTPLATE` etc... using `npc.setEquipment(EquipmentAction action, ItemStack itemStack)`
* `Teleportation` - Teleport the NPC in different locations using `npc.teleport(Location location)`
* `Removing` - Delete the NPC using `NpcAPI.removeNPC(NPC npc)`
* `Event` - Detect when the player interacts with a NPC using the `NPCInteractEvent`, you can also detect the type of interaction by getting the EventAction
* `Movement` - Move the npc to a specified location using `npc.setTarget(Location target, Float speed, Double distance_stop)` `or npc.setTarget(Entity entity, Float speed, Double distance_stop)`

# Value and Signature (Skins related)

As you just read, the npc needs a `value` and a `signature` to be created.
Those 2 can be filled by following these two instructions
* Enter `https://api.mojang.com/users/profiles/minecraft/USERNAME` (Replace `USERNAME` with the username of the deserved skin) and copy the `id`
* Enter `http://sessionserver.mojang.com/session/minecraft/profile/ID?unsigned=false` (Replace `ID` with the copied id) and copy value and signature