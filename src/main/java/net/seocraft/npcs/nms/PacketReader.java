package net.seocraft.npcs.nms;

import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import net.seocraft.npcs.NpcAPI;
import net.seocraft.npcs.NPC;
import net.seocraft.npcs.enums.EventAction;
import net.seocraft.npcs.NPCInteractEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class PacketReader extends ChannelDuplexHandler {
    private Player p;

    public PacketReader(final Player p) {
        this.p = p;
    }

    @Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
        super.write(ctx, msg, promise);
    }

    @Override
    public void channelRead(ChannelHandlerContext context, Object packet) throws Exception {
        if (packet.getClass().getSimpleName().equalsIgnoreCase("PacketPlayInUseEntity")) {
            Integer id = Integer.parseInt(ReflectionUtil.getFieldValue(packet, "a").toString());
            String action = ReflectionUtil.getFieldValue(packet, "action").toString();
            for (NPC npc : NpcAPI.getNPCS()) {
                if (npc.getID().equals(id)) {
                    if (action.equalsIgnoreCase("ATTACK")) {
                        Bukkit.getServer().getPluginManager().callEvent(new NPCInteractEvent(p, npc, EventAction.LEFT_CLICK));
                    } else if (action.equalsIgnoreCase("INTERACT")) {
                        Bukkit.getServer().getPluginManager().callEvent(new NPCInteractEvent(p, npc, EventAction.RIGHT_CLICK));
                    }
                }
            }
        }
        super.channelRead(context, packet);
    }
}