package net.seocraft.npcs.enums;

public enum EventAction {
    RIGHT_CLICK, LEFT_CLICK
}