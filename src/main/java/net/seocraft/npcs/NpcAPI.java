package net.seocraft.npcs;

import net.seocraft.npcs.nms.PacketInjector;
import net.seocraft.npcs.nms.ReflectionUtil;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

public class NpcAPI extends JavaPlugin implements Listener {

    private static NpcAPI instance;
    private static List<NPC> npcs;
    private static PacketInjector packetInjector;

    public static NpcAPI getInstance() {
        return instance;
    }

    public static List<NPC> getNPCS() {
        return npcs;
    }

    @Deprecated
    public static PacketInjector getPacketInjector() {
        return packetInjector;
    }

    public static NPC createNPC(String name, String value, String signature, Location location, Boolean name_visible, Player receiver) {
        NPC npc = ReflectionUtil.newNPC(name, value, signature, location, receiver, name_visible);
        npc.spawn();
        npcs.add(npc);
        return npc;
    }

    public static void removeNPC(NPC npc) {
        npcs.remove(npc);
        npc.despawn();
    }

    @Override
    public void onEnable() {
        instance = this;
        npcs = new ArrayList<>();
        ReflectionUtil.init();
        packetInjector = new PacketInjector();
        getServer().getPluginManager().registerEvents(this, this);
    }

    @Override
    public void onDisable() {
        if (!getNPCS().isEmpty()) {
            for (NPC npc : getNPCS()) {
                if (npc.isMoving()) {
                    npc.getWitch().remove();
                }
            }
        }
    }
}